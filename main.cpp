﻿// Подключение графической библиотеки
#include <SFML/Graphics.hpp>
#include <thread>
#include <chrono>
#include <moon.hpp>
#include <iostream>
#include <earth.hpp>

#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")



using namespace std::chrono_literals;

int main()
{

    float t = 0;
    float sx = 550;
    float sy = 430;
   
    // Создание окна с известными размерами и названием
    sf::RenderWindow window(sf::VideoMode(800, 500), " MOVEMENT OF MOON ");

    // Подгрузка фонового изображения
    sf::Texture texture;
    if (!texture.loadFromFile("img/back1.jpg"))
    {
        std::cout << "ERROR when loading back1.jpg" << std::endl;
        return false;
    }
    sf::Sprite back;
    back.setTexture(texture);

    
    // Добавление иконки
    sf::Image icon;
    if (!icon.loadFromFile("img/earth32.png"))
    {
        return -1;
    }
    window.setIcon(48, 48, icon.getPixelsPtr());
    


    // Генерация объектов
    std::vector<kr::Earth*> earths;
    earths.push_back(new kr::Earth(320, 200, 200));
    
  

    // Подгрузка картинок и завершение программы, если они не загрузились
    for (const auto& earth : earths)
        if (!earth->Setup())
            return -1;

     kr::Moon* moon = nullptr;

    // Цикл работает до тех пор, пока окно открыто
    while (window.isOpen())
    {
        // Переменная для хранения события
        sf::Event event;
        // Цикл по всем событиям
        while (window.pollEvent(event))
        {
            // Обработка событий
            // Если нажат крестик, то
            if (event.type == sf::Event::Closed)
                // окно закрывается
                window.close();
        }


        
        if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
        {
            // Дребезг контактов
            
            sf::Vector2i mp = sf::Mouse::getPosition(window);

            float ay = mp.y - sy;
            float ax = mp.x - sx;

            float angle = acos(ax / sqrt(ax * ax + ay * ay));

            if (moon != nullptr)
                delete moon;

            moon = new kr::Moon(sx, sy, 210, angle);


            if (!moon->Setup())
            {
                delete moon;
                window.close();
                return -1;
            }

            t = 0;
        }

        // Движение луны
        if (moon != nullptr)
        {
            moon->Move(t);
            
        }

        // Вывод на экрна
        window.clear();

        // Вывод фона
        window.draw(back);


        // Вывод на экран земли 
        for (const auto& earth : earths)
            window.draw(*earth->Get());

 
        
        // Вывод луны

        if(moon != nullptr)
            window.draw(*moon->Get());

        // Отобразить на окне все, что есть в буфере
        window.display();

        // https://ravesli.com/urok-129-tajming-koda-vremya-vypolneniya-programmy/
        std::this_thread::sleep_for(40ms);
        t += 0.04;
    }

    if (moon != nullptr)
        delete moon;

    return 0;
}
