#include <moon.hpp>
#include <iostream>

namespace kr
{
	
	Moon::Moon(int x0, int y0, float r, float angle)
	{
		m_x0 = x0;
		m_y0 = y0;
		m_r = r;
		m_angle = (2 * acos(-1) - angle);
		
	}

	bool Moon::Setup()//��������� ����
	{
		if (!m_texture.loadFromFile("img/moon1.png"))
		{
			std::cout << "ERROR when loading moon1.png" << std::endl;
			return false;
		}

		m_moon = new sf::Sprite();
		m_moon->setTexture(m_texture);
		m_moon->setOrigin(m_r, m_r);
		m_moon->setPosition(m_x, m_y);

		return true;
	}

	Moon::~Moon()
	{
		if (m_moon != nullptr)
			delete m_moon;
	}

	void Moon::Move(float t)
	{
		
		m_x = m_x0 + m_r * cos(m_angle * t/8);
		m_y = m_y0 + m_r * sin(m_angle * t/8);
		m_moon->setPosition(m_x, m_y);
	}

	sf::Sprite* Moon::Get() { return m_moon; }//����� get ����� �������� ���������� ������

	int Moon::GetX() { return m_x; }
	int Moon::GetY() { return m_y; }
	float Moon::GetR() { return m_r; }
}