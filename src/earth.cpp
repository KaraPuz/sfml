#include <earth.hpp>

namespace kr//������������ ����
{
	Earth::Earth(int x, int y, float r)
	{
		m_x = x;
		m_y = y;
		m_r = r;
	}

	bool Earth::Setup()
	{

		if (!m_texture.loadFromFile("img/earth.png"))
		{
			std::cout << "ERROR when loading earth.png" << std::endl;
			return false;
		}


		m_earth = new sf::Sprite();
		m_earth->setTexture(m_texture);
		m_earth->setOrigin(m_r, m_r);
		m_earth->setPosition(m_x, m_y);//������������� ����� � ������������ �����
		m_earth->setScale(0.3, 0.3);

		return true;
	}

	Earth::~Earth()
	{
		if (m_earth != nullptr)
			delete m_earth;
	}

	sf::Sprite* Earth::Get() { return m_earth; }//����� get ����� �������� ���������� ������


	int Earth::GetX() { return m_x; }
	int Earth::GetY() { return m_y; }
	float Earth::GetR() { return m_r; }
}
