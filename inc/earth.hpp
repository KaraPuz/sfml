#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>//�������� ��� ���������

namespace kr
{
	class  Earth
	{
	public:
		Earth(int x, int y, float r);//����� �������� ��� ���������� � ������
		~Earth();

		bool Setup();

		sf::Sprite* Get();
		int GetX();
		int GetY();
		float GetR();

	private:
		int m_x, m_y;
		float m_r;

		sf::Texture m_texture;
		sf::Sprite* m_earth = nullptr;
	};
}

